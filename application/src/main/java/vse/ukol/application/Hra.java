package vse.ukol.application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

public class Hra {
    private List<Mesto> seznamMest;     
    private boolean konecHry = false;
    private int celkoveSkore;
    private int dilciSkore;
    private Mesto hledaneMesto;
    private int i=0;


    public Hra() {
    		//Založení měst a přidání do listu
    		Mesto praha = new Mesto("Praha", 274, 189);
    		Mesto cb = new Mesto("České Budějovice", 275, 356);
    		Mesto usti = new Mesto("Ústí nad Labem", 229, 98);
    		Mesto liberec = new Mesto("Liberec", 333, 78);
    		Mesto hk = new Mesto("Hradec Králové", 421, 167);
    		Mesto pardubice = new Mesto("Pardubice", 409, 194);
    		Mesto ostrava = new Mesto("Ostrava", 666, 217);
    		Mesto olomouc = new Mesto("Olomouc", 571, 265);
    		Mesto zlin = new Mesto("Zlín", 602, 328);
    		Mesto brno = new Mesto("Brno", 494, 329);
    		Mesto jihlava = new Mesto("Jihlava", 396, 295);
    		Mesto plzen = new Mesto("Plzeň", 160, 238);
    		Mesto kv = new Mesto("Karlovy Vary", 110, 168);
    	
        seznamMest = new ArrayList<Mesto>();
        seznamMest.add(praha); seznamMest.add(cb); seznamMest.add(usti);
        seznamMest.add(liberec); seznamMest.add(hk); seznamMest.add(pardubice);
        seznamMest.add(ostrava); seznamMest.add(olomouc); seznamMest.add(zlin);
        seznamMest.add(brno); seznamMest.add(jihlava); seznamMest.add(plzen);
        seznamMest.add(kv);
    }

    /* TODO - metoda pro přepočítávání souřadnic, aby bylo možné města nahrávat ze souboru například
    public void prepocitejSouradnice() {
    		for (int i = 0; i < seznamMest.size(); i++) {
    			Mesto mesto = seznamMest.get(i);
			double novaSouradniceX = ...
			double novaSouradniceY = ...
    			mesto.setSouradniceX(novaSouradniceX);
    			mesto.setSouradniceY(novaSouradniceY);
    		}
    }
    */
    
    public String getCelkoveSkore() {
    		String navrat = celkoveSkore + " bodů";
		return navrat;
	}

	public String getDilciSkore() {
		String navrat = dilciSkore + " bodů";
		return navrat;
	}

	public double getHledaneX() {
    		return hledaneMesto.souradniceX;
    }
	
	public double getHledaneY() {
		return hledaneMesto.souradniceY;
}
    
    public boolean konecHry() {
        return konecHry;
    }
      
 
    public void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }
    
    public String vyberMesto() {
    		Collections.shuffle(seznamMest);
    		if (i < seznamMest.size()) {
    			hledaneMesto = seznamMest.get(i);
    			i++;
    			return seznamMest.get(i-1).getNazev();
    		} else {
    			return "Hra skončila";
    		}
    }
     
    public String toString() {
		return "hra, vole";
    }
    
    public void udelBody(double vzdalenost) {
    		if (vzdalenost < 8) {
    			dilciSkore = 5;
    		} else if (vzdalenost < 17) {
    			dilciSkore = 3;
    		} else if (vzdalenost < 30) {
    			dilciSkore = 1;
    		} else {
    			dilciSkore = 0;
    		}
    		celkoveSkore += dilciSkore;
    }

    //spočítá vzdálenost mezi odhadem a realitou, @param - reálné souřadnice odhadu
	public void spocitejVzdalenost(double x, double y) {
		for (int i = 0; i < seznamMest.size(); i++) {
			if (seznamMest.get(i).getNazev().equals(hledaneMesto.getNazev())) {
				double vzdalenost = Math.sqrt(Math.pow(x - hledaneMesto.getSouradniceX(), 2) + Math.pow(x - hledaneMesto.getSouradniceX(), 2));
				//System.out.println(vzdalenost);
				udelBody(vzdalenost);
			}
		}
		
	}
     
    
}

