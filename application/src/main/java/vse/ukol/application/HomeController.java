package vse.ukol.application;

import java.util.Observable;
import java.util.Observer;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class HomeController implements Observer {
	
	@FXML ImageView mapa = new ImageView();
	@FXML ImageView odhad = new ImageView();
	@FXML ImageView realita = new ImageView();
	@FXML TextArea mesto;
	@FXML TextArea dilciBody = new TextArea();
	@FXML TextArea celkoveBody = new TextArea();
	private Hra hra = new Hra();
	private String hledaneMesto;
	
	public void inicializuj() {
	}
	
	@FXML
	public void move() {
		schovatTecky();
		hledaneMesto = hra.vyberMesto();
		mesto.setText(hledaneMesto);
		mapa.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				double souradniceX = event.getX()+10.0;
				double souradniceY = event.getY()-15;
				//System.out.println("X "+event.getX()+", Y "+event.getY());
				odhad.setX(souradniceX);
				odhad.setY(souradniceY);
				odhad.setVisible(true);
				realita.setX(hra.getHledaneX());
				realita.setY(hra.getHledaneY());
				realita.setVisible(true);
				hra.spocitejVzdalenost(souradniceX, souradniceY);
				dilciBody.setText(hra.getDilciSkore());
				celkoveBody.setText(hra.getCelkoveSkore());
				;
			}
		});
	}
	
	public void ukoncitHru() {
		hra.setKonecHry(true);
		System.exit(0);
	}
	
	public void schovatTecky() {
		realita.setVisible(false);
		odhad.setVisible(false);
		realita.setX(0);
		realita.setY(30);
		odhad.setX(0);
		odhad.setY(40);
	}

	public void update(Observable o, Object arg) {		
		
	}
	
	
	
}